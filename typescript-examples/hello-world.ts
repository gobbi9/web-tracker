class Student {
  fullName: string;
  constructor(public firstName: string, public middleInitial: string, public lastName: string) {
    this.fullName = firstName + " " + middleInitial + " " + lastName;
  }
}

interface Person {
  firstName: string;
  lastName: string;
}

function greeter(person: Person) {
  return "Hello, " + person.firstName + " " + person.lastName;
}

let user = new Student("Jane", "M.", "User");

console.log(user.fullName);

document.body.innerHTML = greeter(user);

//types: boolean, number, Array<T>, any (default)

let color1 = 0x1;
let color2 = 0xF;

console.log(`0x${(color2 + color1).toString(16)}`);

let generics: Array<any>;

let list:Array<string> = ["1", "2"];
let list2:string[] = ["1", "2"]

for (let el of list)
  console.log(el);

let tuple: [string, number];

// https://stackoverflow.com/questions/41870411/object-index-key-type-in-typescript
interface CustomMap<B> {
  [key: string]: B;
}

class Test {
  private dictionary: CustomMap<string>;

  constructor() {
    this.dictionary = {}
    this.dictionary[11] = "numeric-index";
    this.dictionary["11"] = "string-index"

    console.log(this.dictionary[11], this.dictionary["11"]);
  }
}

let t = new Test();

let anotherTuple: [string, number, any, any];
let listOfTuples: Array<[string, number, any, any]>;

// ENUMS
enum X {Right = 1, Left = -1}
enum Y {Up = 1, Down = -1}

let right: X = X.Right;
let left: X = X.Left;

console.log(Y[-1]);

//void

function warnUser(): void {
    alert("This is my warning message");
}

// never
// Function returning never must have unreachable end point
function error(message: string): never {
    throw new Error(message);
}

// Inferred return type is never
function fail() {
    return error("Something failed");
}

// Function returning never must have unreachable end point
function infiniteLoop(): never {
    while (true) {
    }
}

// type casting/assertion

let someValue: any = "this is a string";
let strLength: number = (<string>someValue).length;

let someValue2: any = "this is a string";
let strLength2: number = (someValue2 as string).length;

// let, var, const

// protected: obj._prop , private: obj.__prop
//obj.prop or obj.prop()
//obj.prop("value") //+validation

// better import: https://stackoverflow.com/questions/34925992/how-to-avoid-imports-with-very-long-relative-paths-in-angular-2
