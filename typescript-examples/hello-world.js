var Student = /** @class */ (function () {
    function Student(firstName, middleInitial, lastName) {
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    }
    return Student;
}());
function greeter(person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}
var user = new Student("Jane", "M.", "User");
console.log(user.fullName);
document.body.innerHTML = greeter(user);
//types: boolean, number, Array<T>, any (default)
var color1 = 0x1;
var color2 = 0xF;
console.log("0x" + (color2 + color1).toString(16));
var generics;
var list = ["1", "2"];
var list2 = ["1", "2"];
var tuple;
var Test = /** @class */ (function () {
    function Test() {
        this.dictionary = {};
        this.dictionary[11] = "numeric-index";
        this.dictionary["11"] = "string-index";
        console.log(this.dictionary[11], this.dictionary["11"]);
    }
    return Test;
}());
var t = new Test();
var anotherTuple;
var listOfTuples;
// ENUMS
var X;
(function (X) {
    X[X["Right"] = 1] = "Right";
    X[X["Left"] = -1] = "Left";
})(X || (X = {}));
var Y;
(function (Y) {
    Y[Y["Up"] = 1] = "Up";
    Y[Y["Down"] = -1] = "Down";
})(Y || (Y = {}));
var right = X.Right;
var left = X.Left;
console.log(Y[-1]);
//void
function warnUser() {
    alert("This is my warning message");
}
// never
// Function returning never must have unreachable end point
function error(message) {
    throw new Error(message);
}
// Inferred return type is never
function fail() {
    return error("Something failed");
}
// Function returning never must have unreachable end point
function infiniteLoop() {
    while (true) {
    }
}
// type casting/assertion
var someValue = "this is a string";
var strLength = someValue.length;
var someValue2 = "this is a string";
var strLength2 = someValue2.length;
// let, var, const
