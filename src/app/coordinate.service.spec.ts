import { TestBed, inject } from '@angular/core/testing';

import { CoordinateService } from './coordinate.service';
import { HttpClientModule } from '@angular/common/http';

describe('CoordinateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoordinateService],
      imports: [
        HttpClientModule
      ]
    });
  });

  it('should be created', inject([CoordinateService], (service: CoordinateService) => {
    expect(service).toBeTruthy();
  }));
});
