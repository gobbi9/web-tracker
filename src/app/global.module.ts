import { NgModule, ModuleWithProviders, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoordinateService } from 'app/coordinate.service';
import { MapComponent } from 'app/map/map.component';
import { StatusComponent } from 'app/status/status.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MapComponent, StatusComponent],
  exports: [MapComponent, StatusComponent],
  providers: [CoordinateService]
})
// definition of singleton services and components
export class GlobalModule {

  constructor(@Optional() @SkipSelf() parentModule: GlobalModule) {
    if (parentModule) {
      throw new Error(
        'GlobalModule is already loaded. Import it in the TrackerModule only');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: GlobalModule,
      providers: [
        CoordinateService
      ]
    };
  }
}
