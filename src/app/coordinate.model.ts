export interface Coordinate {
  captured_at: string;
  latitude: string;
  longitude: string;
  received_at: string;
  serial_number: number;
}
