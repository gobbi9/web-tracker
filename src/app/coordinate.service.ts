import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment as env } from 'environments/environment';
import { Coordinate } from 'app/coordinate.model';

@Injectable()
export class CoordinateService {

  apiUrl: string
  requestIntervalInSeconds: number

  constructor(private http: HttpClient) {
    this.apiUrl = env.apiUrl;
    this.requestIntervalInSeconds = 10;
    console.log("API URL: " + this.apiUrl);
  }

  setRequestIntervalInSeconds(newIntervalInSeconds: number): void {
    this.requestIntervalInSeconds = newIntervalInSeconds;
  }

  getRequestIntervalInSeconds(): number {
    return this.requestIntervalInSeconds;
  }

  // 1. connect to arduino
  connect(): Observable<HttpResponse<null>> {
    return this.http.get<null>(`${this.apiUrl}/connect?sender=arduino&receiver=web1`, { observe: 'response' });
  }

  // 4. get coordinates from server
  getCoordinates(): Observable<HttpResponse<any>> {
    return this.http.get<Coordinate[]>(`${this.apiUrl}/coordinates?sender=arduino&receiver=web1`, { observe: 'response' });
  }

  // 5. disconnect from arduino
  disconnect(): Observable<HttpResponse<null>> {
    return this.http.get<null>(`${this.apiUrl}/disconnect?sender=arduino&receiver=web1`, { observe: 'response' });
  }

  // /history, start and end params in the format: 2018-04-27T00:00:00.000 and in UTC time
  history(start: string, end: string): Observable<HttpResponse<any>> {
    return this.http.get<Coordinate[]>(`${this.apiUrl}/history/arduino?start=${start}&end=${end}`, { observe: 'response' });
  }
}
