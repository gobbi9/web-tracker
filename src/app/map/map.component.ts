import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { CoordinateService } from 'app/coordinate.service';
import { Coordinate } from 'app/coordinate.model';
import { Observable } from 'rxjs/Rx';
import { ClientState } from 'app/clientstate.enum';

// Google Maps
import { } from '@types/googlemaps';

@Component({
  selector: 'tracker-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  outputText: string = '....'

  clientState: ClientState

  coordinates: Coordinate[] = []
  // markers: google.maps.Marker[] = []

  timer: any
  timerObj: any

  // Google Maps
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;
  mapProp: any

  constructor(private coordinateService: CoordinateService) { }

  ngOnInit() {
    this.timer = Observable.timer(1000, this.coordinateService.getRequestIntervalInSeconds() * 1000);
    this.clientState = ClientState.Disconnected;
    this.mapProp = {
      center: { lat: -22.858675, lng: -43.232164 },
      zoom: 19,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, this.mapProp);
    this.connect();
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    this.disconnect();
  }

  refreshTimer() {
    this.timer = Observable.timer(1000, this.coordinateService.getRequestIntervalInSeconds() * 1000);
  }

  connect() {
    this.coordinateService.connect()
      .subscribe(resp => {
        this.clientState = ClientState.Connected;

        if (this.timerObj != null && this.timerObj != undefined)
          this.timerObj.unsubscribe();
        this.timerObj = this.timer.subscribe(() => {
          this.getCoordinates();
          if (this.clientState == ClientState.Disconnected)
            this.timerObj.unsubscribe();
        });

        console.log(`Connected to server as web1 (status: ${resp.status}) at ${new Date()}`);
      }, error => {
        console.log(`Could not connect to server as web1 at ${new Date()}`);
        this.clientState = ClientState.Disconnected;
      });
  }

  getCoordinates() {
    if (this.clientState == ClientState.Disconnected)
      console.log('Client is disconnected, must call /connect first');
    else {
      this.coordinateService.getCoordinates()
        .subscribe(resp => {
          let newCoordinates: Coordinate[] = resp.body.coordinates;
          this.coordinates = this.coordinates.concat(newCoordinates);
          this.clientState = ClientState.Receiving;

          let lastIndex = this.coordinates.length - 1;
          let location = this.coordinates.length == 0 ?
            this.mapProp.center :
            new google.maps.LatLng(
              this.stringToCoordinate(this.coordinates[lastIndex].latitude),
              this.stringToCoordinate(this.coordinates[lastIndex].longitude)
            );

          // if (newCoordinates.length > 0)
          //   this.map.setZoom(19);

          if (newCoordinates.length > 0)
            this.map.panTo(location);

          this.coordinates.forEach(coordinate => {
            new google.maps.Marker({
              position: new google.maps.LatLng(
                this.stringToCoordinate(coordinate.latitude),
                this.stringToCoordinate(coordinate.longitude)
              )
              , map: this.map
              , title: new Date(coordinate.captured_at + "Z").toLocaleString("pt-BR")
            });
          });

          console.log(`Received ${newCoordinates.length} new coordinates (status: ${resp.status}) at ${new Date()}, tick: ${this.coordinateService.getRequestIntervalInSeconds()} seconds`);
        }, error => {
          this.clientState = ClientState.Connected;
          console.log(`Could not get new coordinates at ${new Date()}, tick: ${this.coordinateService.getRequestIntervalInSeconds()} seconds`);
        });
    }
  }

  disconnect() {
    this.coordinateService.disconnect()
      .subscribe(resp => {
        this.clientState = ClientState.Disconnected;
        console.log(`Disconnected from arduino as web1 (status: ${resp.status}) at ${new Date()}`);
      }, error => {
        this.clientState = ClientState.Receiving;
        console.log(`Could not disconnect at ${new Date()}`);
      });
  }

  private stringToCoordinate(latOrLng: string) {
    return parseInt(latOrLng) / 1000000;
  }

}
