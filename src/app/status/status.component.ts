import { Component, OnInit, Input } from '@angular/core';
import { ClientState } from 'app/clientstate.enum';
import { MapComponent } from 'app/map/map.component';

@Component({
  selector: 'tracker-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {
  ClientState = ClientState

  @Input() status: ClientState

  constructor(private mapComponent: MapComponent) { }

  ngOnInit() {
  }

  connect() {
    this.mapComponent.connect();
  }

  disconnect() {
    this.mapComponent.disconnect();
  }

}
