import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MapComponent } from './map/map.component';
import { MainComponent } from './main/main.component';
import { CoordinateService } from './coordinate.service';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HistoryComponent } from './history/history.component';
import { AboutComponent } from './about/about.component';
import { TrackerRoutingModule } from './/tracker-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { GlobalModule } from 'app/global.module';
import { CalendarModule } from 'primeng/calendar';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  { path: '', component: MapComponent },
  { path: 'historico', component: HistoryComponent },
  { path: 'sobre', component: AboutComponent }
];

@NgModule({
  declarations: [
    MainComponent, NavbarComponent, HistoryComponent, AboutComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule.forRoot(),
    TrackerRoutingModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    ReactiveFormsModule,
    GlobalModule.forRoot(),
    CalendarModule,
    CommonModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [MainComponent]
})
export class TrackerModule {
}
