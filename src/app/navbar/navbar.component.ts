import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { CoordinateService } from 'app/coordinate.service';
import { MapComponent } from 'app/map/map.component';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'tracker-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  animations: [
    trigger('collapse', [
      state('open', style({
        opacity: '1'
      })),
      state('closed', style({
        opacity: '0',
        display: 'none',
      })),
      transition('closed => open', animate('400ms ease-in')),
      transition('open => closed', animate('100ms ease-out'))
    ])
  ],
  providers: [MapComponent]
})
export class NavbarComponent implements OnInit {
  form: FormGroup;
  isNavbarCollapsed = true;
  _isNavbarCollapsedAnim = 'closed';
  public route: string = "";
  public requestIntervalInSeconds: number
  public requestIntervalInSecondsInput: number

  constructor(location: Location, router: Router, private coordinateService: CoordinateService, private mapComponent: MapComponent, private formBuilder: FormBuilder) {
    router.events.subscribe((val) => {
      this.route = location.path();
    });
  }

  ngOnInit() {
    this.onResize(window);
    this.requestIntervalInSeconds = this.coordinateService.getRequestIntervalInSeconds();
    this.requestIntervalInSecondsInput = null;
    this.form = this.formBuilder.group({
      requestInterval: new FormControl('', [
        Validators.required,
        Validators.min(1)
      ])
    });
  }

  showRequestInterval() {
    this.requestIntervalInSeconds = this.requestIntervalInSecondsInput = this.coordinateService.getRequestIntervalInSeconds();
  }

  undoShowRequestInterval() {
    this.requestIntervalInSecondsInput = null;
  }

  updateRequestInterval(requestIntervalInSecondsInput) {
    console.log("param ", requestIntervalInSecondsInput);
    this.requestIntervalInSeconds = requestIntervalInSecondsInput;
    this.coordinateService.setRequestIntervalInSeconds(this.requestIntervalInSeconds);
    this.mapComponent.refreshTimer();
    this.requestIntervalInSecondsInput = null;
  }

  @HostListener('window:resize', ['$event.target'])
  onResize(event) {
    if (event.innerWidth > 990) {
      //need to set this to 'open' for large screens to show up because of opacity in 'closed' animation.
      this._isNavbarCollapsedAnim = 'open';
      this.isNavbarCollapsed = true;
    } else {
      // comment this line if you don't want to collapse the navbar when window is resized.
      // this._isNavbarCollapsedAnim = 'closed';
    }
  }

  toggleNavbar(): void {
    if (this.isNavbarCollapsed) {
      this._isNavbarCollapsedAnim = 'open';
      this.isNavbarCollapsed = false;
    } else {
      this._isNavbarCollapsedAnim = 'closed';
      this.isNavbarCollapsed = true;
    }
  }

  get isNavbarCollapsedAnim(): string {
    return this._isNavbarCollapsedAnim;
  }

}
