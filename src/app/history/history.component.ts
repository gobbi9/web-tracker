import { Component, OnInit, ViewChild } from '@angular/core';
import { Coordinate } from 'app/coordinate.model';
import { CoordinateService } from 'app/coordinate.service';

@Component({
  selector: 'tracker-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  coordinates: Coordinate[] = []

  start: Date
  end: Date

  br: any

  // Google Maps
  @ViewChild('historymap') gmapElement: any;
  map: google.maps.Map;
  mapProp: any

  constructor(private coordinateService: CoordinateService) { }

  ngOnInit() {
    this.mapProp = {
      center: { lat: -22.858675, lng: -43.232164 },
      zoom: 19,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, this.mapProp);
    this.br = {
      firstDayOfWeek: 0,
      dayNames: ["Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado"],
      dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
      dayNamesMin: ["Do", "Se", "Te", "Qua", "Qui", "Se", "Sa"],
      monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
      monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
      today: 'Hoje',
      clear: 'Limpar'
    };
  }

  refresh() {
    let startUTCNoTimeZone = this.start.toISOString().replace("Z", "");
    let endUTCNoTimeZone = this.end.toISOString().replace("Z", "");

    this.history(startUTCNoTimeZone, endUTCNoTimeZone);
  }

  history(start: string, end: string) {
    this.coordinateService.history(start, end)
      .subscribe(resp => {
        let coordinates: Coordinate[] = resp.body.coordinates;
        this.coordinates = coordinates;

        this.coordinates.forEach(coordinate => {
          new google.maps.Marker({
            position: new google.maps.LatLng(
              this.stringToCoordinate(coordinate.latitude),
              this.stringToCoordinate(coordinate.longitude)
            )
            , map: this.map
            , title: new Date(coordinate.captured_at + "Z").toLocaleString("pt-BR")
          });
        });

        this.map.panTo(coordinates.length == 0 ? this.mapProp.center : new google.maps.LatLng(
          this.stringToCoordinate(coordinates[0].latitude),
          this.stringToCoordinate(coordinates[0].longitude)
        ));

        console.log(`Retrieved ${coordinates.length} coordinates (status: ${resp.status})`);
      }, error => {
        console.log(`Could not retrieve coordinates`);
      });
  }

  private stringToCoordinate(latOrLng: string) {
    return parseInt(latOrLng) / 1000000;
  }
}
