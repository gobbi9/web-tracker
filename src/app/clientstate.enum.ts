export enum ClientState {
  Connected = 0, // waiting for coordinates
  Receiving = 1, // last call for coordinates did not return an error
  Disconnected = 2 // initial state, after window is closed
}
