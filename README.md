# Frontend em Angular do TCC

Objetivo: possibilitar o rastreamento sem um dispositivo Android.

## Rodando a aplicação no Ubuntu/Mint

1. [Instalar docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
2. [Instalar docker-compose](https://docs.docker.com/compose/install/)

## Iniciando a aplicação (branch dev)

> Só eu posso pushar pra `master` por motivos de segurança, você pode trabalhar na branch `dev`

```bash
git clone -b dev git@gitlab.com:gobbi9/tracker.git
# git clone -b dev https://gitlab.com/gobbi9/tracker.git
cd tracker
uid=`id -u` gid=`id -g` docker-compose up --build -d
cd ..
git clone -b dev git@gitlab.com:gobbi9/web-tracker.git
cd web-tracker
uid=`id -u` gid=`id -g` docker-compose up --build -d
```

## Acessando no modo de desenvolvimento

URLS: <https://web.tracker.127.0.0.1.xip.io/> ou <http://localhost:4200>

## Referências

1. <https://ng-bootstrap.github.io/#/components/accordion/examples>
2. <https://medium.com/codingthesmartway-com-blog/building-an-angular-5-project-with-bootstrap-4-and-firebase-4504ff7717c1>
3. <https://github.com/ng-bootstrap/ng-bootstrap/issues/738>

---

# WebTracker

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
